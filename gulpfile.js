var gulp = require('gulp'),
  sass = require('gulp-sass'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  runSequence = require('gulp-run-sequence'),
  imagemin = require('gulp-imagemin'),
  cssmin = require('gulp-cssmin'),
  uglifycss = require('gulp-uglifycss');

gulp.task('sass', function () {
    gulp.src('./src/scss/**/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(sass({
          outputStyle: 'compressed'
      }))
      .pipe(uglifycss({
          "uglyComments": true
      }))
      .pipe(cssmin())
      .pipe(gulp.dest('./css'));
});

gulp.task('sass-dev', function () {
  gulp.src('./src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({
      outputStyle: 'expanded'
    }))
    .pipe(gulp.dest('./css'));
});

gulp.task('watch', function () {
    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['main-js']);
});


gulp.task('vendor-js', function() {
  gulp.src('./src/vendor/js/*.js')
    .pipe(uglify({mangle: false}))
    .pipe(concat('vendor.min.js'))
    .pipe(gulp.dest('./js/vendor'));
});

gulp.task('js', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./js'));
});

gulp.task('vendor-css', function() {
  gulp.src('./src/vendor/css/**/*.css')
    .pipe(concat('vendor.min.css'))
    .pipe(uglifycss({
      "uglyComments": true
    }))
    .pipe(cssmin())
    .pipe(gulp.dest('./css/vendor'));
});

gulp.task('main-js', function() {
  gulp.src('./src/js/*.js')
    .pipe(uglify({mangle: false}))
    .pipe(concat('all.min.js'))
    .pipe(gulp.dest('./js'));
});

gulp.task('assets', function () {
    return gulp.src('./src/assets/**/*')
        .pipe(gulp.dest('./assets'))
});

gulp.task('copy-fonts', function() {
    gulp.src('./src/fonts/**/*')
      .pipe(gulp.dest('./fonts'));
});

gulp.task('images', function(cb) {
    gulp.src(['./src/img/**/*.png','./src/img/**/*.svg','./src/img/**/*.jpg','./src/img/**/*.gif','./src/img/**/*.jpeg'])
      .pipe(imagemin({
          optimizationLevel: 3,
          progressive: true,
          interlaced: true
      }))
      .pipe(gulp.dest('./img/')).on('end', cb).on('error', cb);
});

gulp.task('images-develop', function(cb) {
    gulp.src(['./src/img/**/*.png','./src/img/**/*.svg','./src/img/**/*.jpg','./src/img/**/*.gif','./src/img/**/*.jpeg'])
      .pipe(gulp.dest('./img/')).on('end', cb).on('error', cb);
});

// Main tasks
gulp.task('default', function() {
    runSequence('sass', 'main-js', 'vendor-css', 'images', 'images-develop', 'vendor-js', 'copy-fonts');
});

gulp.task('dev', ['sass-dev', 'watch', 'vendor-css', 'images', 'images-develop', 'main-js',  'vendor-js', 'copy-fonts'], function() {
});